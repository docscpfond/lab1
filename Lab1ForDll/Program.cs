﻿using System;
using Lab1DLL;

namespace Lab1ForDll
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Student()
            {
                age = 300,
                birthday = new DateTime(1999, 01, 1),
                gender = Gender.Male,
                nationality = Nationality.Ukrainian,
                Name = "Viktor",
                group = "SNs-42",
                numberBook = 666,
                pasportNumber = 56412354,
                height = 190,
                mass = 80
                
            };
            Console.WriteLine(a);
        }
    }
}
